<?php

/**
 * Created by PhpStorm.
 * User: owner
 * Date: 30/08/2016
 * Time: 12:22 PM
 */

require_once "../vendor/autoload.php";

use PHPUnit\Framework\TestCase;
require_once __DIR__ . "/../Classes/LevyTable.php";
require_once __DIR__ . "/../Classes/MedicareLevyCalculator.php";

class MedicareLevyTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var MedicareLevyCalculator
     */
    private $mlc;
    private $levytbl;

    public function setUp()
    {
        /** @var LevyTable $mockLevyTable */
        $this->levytbl = Mockery::mock(LevyTable::class)
            ->shouldReceive("getLevyRate")
            ->getMock();


        $this->mlc = new MedicareLevyCalculator($this->levytbl);
    }

    public function tearDown(){
        Mockery::close();
    }

    public function testFoo(){
        $this->assertTrue(TRUE);
    }

    public function calculateMedicareLevy()
    {
        $actual = $this->mlc->calculateMedicareLevy("50000", 2016);
    }
}