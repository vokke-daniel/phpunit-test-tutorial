<?php

use PHPUnit\Framework\TestCase;

require_once "../vendor/autoload.php";
require_once __DIR__ . "/../Classes/LevyTable.php";

class LevyTableTest extends PHPUnit_Framework_TestCase
{
    private $levytable;

    public function setUp()
    {
        $this->levytable = new LevyTable();
    }

    /**
     * @expectedException Exception
     */
    public function testGetLevyRateException_v2()
    {
        $this->levytable->getLevyRate("2001");
    }

//    public function testGetLevyRateException()
//    {
//        try {
//            $this->levytable->getLevyRate("2001");
//        } catch (Exception $e) {
//            $err_msg = $e->getMessage();
//        }
//        $this->assertEquals($err_msg, "No levy rate for year '2001'.");
//    }

}