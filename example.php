<?php

require_once "Classes/LevyTable.php";
require_once "Classes/MedicareLevyCalculator.php";

$medicareLevyCalculator = new MedicareLevyCalculator(new LevyTable());
$levy = $medicareLevyCalculator->calculateMedicareLevy("50000", 2016);

echo "The Medicare levy surcharge for 50,000 per annum is $levy";