<?php

/**
 * Class MedicareLevyCalculator
 */
class MedicareLevyCalculator {

	/** $levyTable
	 *
	 * 	Service used to reveal levy rates per year.
	 *
	 * @var LevyTable
	 */
	private $levyTable;

	/** __construct
	 *
	 * 	MedicareLevyCalculator constructor.
	 *
	 * @param LevyTable $levyTable
	 */
	public function __construct(LevyTable $levyTable){
		$this->levyTable = $levyTable;
	}

	/** calculateMedicareLevy
	 *
	 * 	Returns the Medicare levy surcharge for the given year for a given taxable income.
	 *
	 * @param $income
	 * @param $forYear
	 * @return float
	 * @throws Exception
	 */
	public function calculateMedicareLevy($income, $forYear){
		$rate = 0; //$this->levyTable->getLevyRate($forYear);

		return $income * $rate;
	}

}