<?php

/**
 * Class LevyTable
 */
class LevyTable {

	/** $table
	 *
	 * 	Array of rates.
	 *
	 * @var array
	 */
	private $table = array(
		"2016" => 0.05,
		"2017" => 0.75
	);

	/** getLevyRate
	 *
	 * 	Returns the levy rate for the given year. Throws an exception if there
	 * 	is no associated rate with the year.
	 *
	 * @param $forYear
	 * @return float
	 * @throws Exception
	 */
	public function getLevyRate($forYear){
		if (isset($this->table[$forYear])){
			return $this->table[$forYear];
		}

		throw new Exception("No levy rate for year '$forYear'.");
	}

}